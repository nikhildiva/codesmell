package org.informatica.goodcode.bbbloaters.absrpapplication;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PaymentMode {
    UPI(5),
    NEFT(100),
    RTGS(0);

    private final float etaInSeconds;
}
