package org.informatica.goodcode.bbbloaters.absrpapplication;

import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentData;
import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentMode;

public class PaymentService {

    private PaymentService() {
    }

    /**
     * @param paymentData payment params
     * @return status of payment and notification
     */
    public static boolean makePayment(PaymentData paymentData) {

        boolean isPaymentSuccess;
        if (paymentData.getMode().equals(PaymentMode.UPI.name())) {
            //do payment for UPI
            isPaymentSuccess = true;
        } else if (paymentData.getMode().equals(PaymentMode.NEFT.name())) {
            //do payment with NEFT
            isPaymentSuccess = true;
        } else {
            //invalid payment method, return false
            System.out.println("Payment Failed");
            isPaymentSuccess = false;
        }

        return isPaymentSuccess;
    }

}
