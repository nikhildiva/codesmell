package org.informatica.goodcode.bbbloaters.absrpapplication;

public class NotificationService {

    /**
     * @param notificationData notification params
     * @return status of payment and notification
     */
    public static boolean notify(NotificationData notificationData) {

        boolean isNotificationSuccess;
        if (notificationData.notificationMode.equals(NotificationMode.PUSH)) {
            //notify with PUSH
            System.out.println("PUSH:" + notificationData.message);
            isNotificationSuccess = true;
        } else if (notificationData.notificationMode.equals(NotificationMode.EMAIL)) {
            //notify with EMAIL
            System.out.println("EMAIL:" + notificationData.message);
            isNotificationSuccess = true;
        } else {
            System.out.println("Unsupported channel");
            isNotificationSuccess = false;
        }

        return isNotificationSuccess;
    }

}
