package org.informatica.goodcode.bbbloaters.absrpapplication;

public enum NotificationMode {
    EMAIL,
    SMS,
    PUSH;
}
