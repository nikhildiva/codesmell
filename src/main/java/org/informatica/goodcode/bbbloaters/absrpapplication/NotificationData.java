package org.informatica.goodcode.bbbloaters.absrpapplication;

import lombok.Data;

@Data
public class NotificationData {
    String notificationUrl;
    NotificationMode notificationMode;
    String message;
}
