package org.informatica.goodcode.bbbloaters.acocpapplication.payment;

import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentData;

public abstract class PaymentService {

    public abstract boolean makePayment(PaymentData paymentData);
}
