package org.informatica.goodcode.bbbloaters.acocpapplication.notification;

import org.informatica.goodcode.bbbloaters.acocpapplication.NotificationMode;
import org.informatica.goodcode.bbbloaters.acocpapplication.notification.impl.EmailNotificationService;
import org.informatica.goodcode.bbbloaters.acocpapplication.notification.impl.PushNotificationService;
import org.informatica.goodcode.bbbloaters.acocpapplication.notification.impl.SMSNotificationService;

public class NotificationFactory {

    private NotificationFactory() {
    }

    public static NotificationService getService(NotificationMode notificationMode) throws UnsupportedOperationException {
        if (notificationMode == NotificationMode.EMAIL) {
            return new EmailNotificationService();
        } else if (notificationMode == NotificationMode.PUSH) {
            return new PushNotificationService();
        } else if (notificationMode == NotificationMode.SMS) {
            return new SMSNotificationService();
        } else {
            System.out.println("Unsupported notification service");
            throw new UnsupportedOperationException();
        }

    }

}
