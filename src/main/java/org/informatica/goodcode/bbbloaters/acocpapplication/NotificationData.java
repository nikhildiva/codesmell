package org.informatica.goodcode.bbbloaters.acocpapplication;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NotificationData {
    String notificationUrl;
    NotificationMode notificationMode;
    String message;
}
