package org.informatica.goodcode.bbbloaters.acocpapplication.payment;

import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentMode;
import org.informatica.goodcode.bbbloaters.acocpapplication.payment.impl.NEFTPaymentService;
import org.informatica.goodcode.bbbloaters.acocpapplication.payment.impl.RTGSPaymentService;
import org.informatica.goodcode.bbbloaters.acocpapplication.payment.impl.UPIPaymentService;

public class PaymentFactory {

    private PaymentFactory() {
    }

    public static PaymentService getService(PaymentMode mode) throws UnsupportedOperationException {
        if (mode == PaymentMode.UPI) {
            return new UPIPaymentService();
        } else if (mode == PaymentMode.NEFT) {
            return new NEFTPaymentService();
        } else if (mode == PaymentMode.RTGS) {
            return new RTGSPaymentService();
        } else {
            System.out.println("Unsupported notification service");
            throw new UnsupportedOperationException();
        }

    }

}
