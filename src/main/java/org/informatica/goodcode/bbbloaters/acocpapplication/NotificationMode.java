package org.informatica.goodcode.bbbloaters.acocpapplication;

public enum NotificationMode {
    EMAIL,
    SMS,
    PUSH;
}
