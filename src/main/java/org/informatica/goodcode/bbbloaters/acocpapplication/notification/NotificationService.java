package org.informatica.goodcode.bbbloaters.acocpapplication.notification;

import org.informatica.goodcode.bbbloaters.acocpapplication.NotificationData;

public abstract class NotificationService {

    public abstract boolean notifyPayer(NotificationData notificationData);
}
