package org.informatica.goodcode.bbbloaters.acocpapplication.payment;

import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentData;
import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentMode;
import org.informatica.goodcode.bbbloaters.acocpapplication.NotificationData;
import org.informatica.goodcode.bbbloaters.acocpapplication.NotificationMode;
import org.informatica.goodcode.bbbloaters.acocpapplication.notification.NotificationFactory;
import org.informatica.goodcode.bbbloaters.acocpapplication.notification.NotificationService;

public class MakePayment {
    public static void main(String[] args) {
        PaymentService paymentService = PaymentFactory.getService(PaymentMode.NEFT);
        NotificationService notificationService = NotificationFactory.getService(NotificationMode.PUSH);

        String message = String.valueOf(paymentService.makePayment(new PaymentData()));

        notificationService.notifyPayer(new NotificationData("url", NotificationMode.PUSH, message));

    }
}
