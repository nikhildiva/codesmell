package org.informatica.goodcode.bbbloaters.acocpapplication.payment.impl;


import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentData;
import org.informatica.goodcode.bbbloaters.acocpapplication.payment.PaymentService;

public class NEFTPaymentService extends PaymentService {

    @Override
    public boolean makePayment(PaymentData paymentData) {
        return false;
    }
}
