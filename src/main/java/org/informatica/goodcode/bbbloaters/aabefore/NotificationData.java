package org.informatica.goodcode.bbbloaters.aabefore;

import lombok.Data;

@Data
public class NotificationData {
    String notificationUrl;
    NotificationMode notificationMode;
}
