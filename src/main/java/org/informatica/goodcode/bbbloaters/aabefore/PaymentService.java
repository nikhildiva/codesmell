package org.informatica.goodcode.bbbloaters.aabefore;

import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentData;
import org.informatica.goodcode.aalongparameterlist.acencapsulation.PaymentMode;

public class PaymentService {

    private PaymentService() {
    }

    /**
     * @param paymentData      payment params
     * @param notificationData notification params
     * @return status of payment and notification
     */
    public static boolean makePaymentAndNotify(PaymentData paymentData, NotificationData notificationData) {

        boolean isPaymentSuccess;
        if (paymentData.getMode().equals(PaymentMode.UPI.name())) {
            //do payment for UPI
            isPaymentSuccess = true;
        } else if (paymentData.getMode().equals(PaymentMode.NEFT.name())) {
            //do payment with NEFT
            isPaymentSuccess = true;
        } else {
            //invalid payment method, return false
            System.out.println("Payment Failed");
            isPaymentSuccess = false;
        }

        String notification = isPaymentSuccess ? "Success" : "False";


        boolean isNotificationSuccess;
        if (notificationData.notificationMode.equals(NotificationMode.PUSH)) {
            //notify with PUSH
            System.out.println("PUSH:" + notification);
            isNotificationSuccess = true;
        } else if (notificationData.notificationMode.equals(NotificationMode.EMAIL)) {
            //notify with EMAIL
            System.out.println("EMAIL:" + notification);
            isNotificationSuccess = true;
        } else {
            System.out.println("Unsupported channel");
            isNotificationSuccess = false;
        }

        return isPaymentSuccess && isNotificationSuccess;
    }

}
