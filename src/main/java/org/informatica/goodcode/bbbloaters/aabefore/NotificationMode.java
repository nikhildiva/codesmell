package org.informatica.goodcode.bbbloaters.aabefore;

public enum NotificationMode {
    EMAIL,
    SMS,
    PUSH;
}
