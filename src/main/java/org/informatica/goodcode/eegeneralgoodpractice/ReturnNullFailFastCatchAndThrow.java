package org.informatica.goodcode.eegeneralgoodpractice;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReturnNullFailFastCatchAndThrow {

    // Lacks phrasing
    List<String> getListOfStudentsBad() {
        try (Connection connection = DriverManager.getConnection("");
                PreparedStatement preparedStatement = connection.prepareStatement("Select  * from students"))
        { ResultSet rs = preparedStatement.executeQuery();
            List<String> result = new ArrayList<>();
            while (rs.next())
            { result.add(rs.getString(1)); }
            return result; } catch (SQLException e)
        { System.out.println("Connection failed");
            // never return null
            return null; }
    }


    List<String> getListOfStudentsBad2() throws Exception {
        try (
                Connection connection = DriverManager.getConnection("");
                PreparedStatement preparedStatement = connection.prepareStatement("Select  * from students")
        ) {
            ResultSet rs = preparedStatement.executeQuery();
            List<String> result = new ArrayList<>();
            while (rs.next()){
                result.add(rs.getString(1));
            }
            return result;
        } catch (SQLException e) {
            // Never catch and throw or swallow exceptions
            throw new Exception("Connection failed");
        }
    }
    List<String> getListOfStudentsGood() throws SQLException {

        Connection connection = DriverManager.getConnection("");
        PreparedStatement preparedStatement = connection.prepareStatement("Select  * from students");
        ResultSet rs = preparedStatement.executeQuery();
        List<String> result = new ArrayList<>();

        while (rs.next()) {
            result.add(rs.getString(1));
        }
        return result;
    }
}
