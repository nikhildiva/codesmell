package org.informatica.goodcode.eegeneralgoodpractice;

public class CQS {
    // Command and query together.
    public String deleteFileAndReturnContents(){
        // read file and save to string
        String fileContent = "data from file";
        // delete file
        return fileContent;
    }

}
