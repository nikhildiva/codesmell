package org.informatica.goodcode.eegeneralgoodpractice;

public class MagicNumber {

    /**
     * @param number input to be validated
     * @return 0 for even, 1 for odd
     */
    public int isEven(int number) {
        return 1 - (number % 2);
    }
}
