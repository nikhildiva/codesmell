package org.informatica.goodcode.eegeneralgoodpractice;

public class FailFastReturnEarly {

    public float binaryOpBad(int first, int second, char op) {
        float ans = 0;
        switch (op) {
            case '*':
                ans = first * second;
                break;
            case '+':
                ans = first + second;
                break;
            case '/':
                ans = (float) first / second;
                break;
            default:
                System.out.println("Not supported");
        }

        return ans;
    }


    public float binaryOpGood(int first, int second, char op) {
        switch (op) {
            case '*':
                return first * second;
            case '+':
                return first + second;
            case '/':
                if(second == 0){
                    throw new ArithmeticException("Division by zero");
                }
                return (float)first / second;
            default:
                throw new UnsupportedOperationException(op + " not supported yet");
        }
    }
}
