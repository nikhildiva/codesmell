package org.informatica.goodcode.eegeneralgoodpractice;

public class BadPersonClass {
    //1. unnecessary exposure
    public String ssn;
    //2. indirect unnecessary exposure through setters
    private String bankAccount;

    private StringBuilder address;


    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    //3. mutable object reference passed through getter
    public StringBuilder getAddress() {
        return address;
    }
}
