package org.informatica.goodcode.aalongparameterlist.acencapsulation;


import lombok.Data;

@Data
public class PaymentDestination {
    String id;
    String type;
}
