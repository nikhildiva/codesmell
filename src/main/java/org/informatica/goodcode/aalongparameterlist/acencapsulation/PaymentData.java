package org.informatica.goodcode.aalongparameterlist.acencapsulation;


import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PaymentData {
    PaymentSource paymentSource;
    PaymentDestination paymentDestination;
    String mode;
    Float amount;
    LocalDateTime transactionTime;
}
