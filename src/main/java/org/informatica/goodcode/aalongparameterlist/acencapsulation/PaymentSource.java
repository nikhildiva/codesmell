package org.informatica.goodcode.aalongparameterlist.acencapsulation;

import lombok.Data;

/**
 * This class in model for payment source.
 */
@Data
public class PaymentSource {
    String id;
    String location;
}
