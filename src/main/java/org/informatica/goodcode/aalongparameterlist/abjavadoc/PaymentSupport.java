package org.informatica.goodcode.aalongparameterlist.abjavadoc;

import java.time.LocalDateTime;
import java.util.Random;


public class PaymentSupport {

    private PaymentSupport() {
    }

    /**
     * This function processes payment based on parameters and returns payment status
     *
     * @param sourceId        unique id for payment source device
     * @param sourceLocation  device location when payment initiated
     * @param amount          payment amount
     * @param mode            mode of payment - applicable UPI, NEFT, RTGS
     * @param destinationId   unique id for payment destination account
     * @param destinationType type of receiver i.e wallet, account etc
     * @param transactionTime timestamp of txn initiation
     * @return payment status
     */
    public static boolean processPayment(String sourceId,
                                         String sourceLocation,
                                         Float amount,
                                         String mode,
                                         String destinationId,
                                         String destinationType,
                                         LocalDateTime transactionTime) {

        // process payment code

        return new Random().nextBoolean();

    }
}
