package org.informatica.goodcode.ccoopabuse.aabefore.impl;

import org.informatica.goodcode.ccoopabuse.aabefore.Bird;

public class Bat extends Bird {

    @Override
    public void canFly() {
        //fly stuffs
    }

    @Override
    public void canBreath() {
        // breath stuffs
    }

    @Override
    public void hasBeak() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void hasBipeds() {
        // leg stuffs
    }
}
