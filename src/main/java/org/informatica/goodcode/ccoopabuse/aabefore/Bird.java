package org.informatica.goodcode.ccoopabuse.aabefore;

public abstract class Bird {
    public abstract void canFly();
    public abstract void canBreath();
    public abstract void hasBeak();
    public abstract void hasBipeds();
}
