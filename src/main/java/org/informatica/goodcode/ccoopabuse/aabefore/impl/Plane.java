package org.informatica.goodcode.ccoopabuse.aabefore.impl;

import org.informatica.goodcode.ccoopabuse.aabefore.Bird;

public class Plane extends Bird {
    @Override
    public void canFly() {

    }

    @Override
    public void canBreath() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void hasBeak() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void hasBipeds() {

    }
}
