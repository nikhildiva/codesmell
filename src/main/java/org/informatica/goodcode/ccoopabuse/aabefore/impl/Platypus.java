package org.informatica.goodcode.ccoopabuse.aabefore.impl;

import org.informatica.goodcode.ccoopabuse.aabefore.Bird;

public class Platypus extends Bird {

    @Override
    public void canFly() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void canBreath() {
        // breath stuffs
    }

    @Override
    public void hasBeak() {
        // beak stuffs
    }

    @Override
    public void hasBipeds() {
        throw new UnsupportedOperationException();
    }
}
