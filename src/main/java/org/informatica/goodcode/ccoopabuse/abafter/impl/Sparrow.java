package org.informatica.goodcode.ccoopabuse.abafter.impl;


import org.informatica.goodcode.ccoopabuse.abafter.abstraction.FlyableBird;

public class Sparrow extends FlyableBird {

    @Override
    public void canFly() {
        //fly stuffs
    }

    @Override
    public void canBreath() {
        // breath stuffs
    }

    @Override
    public void hasBeak() {
        //beak stuffs
    }

    @Override
    public void hasBipeds() {
        // leg stuffs
    }
}
