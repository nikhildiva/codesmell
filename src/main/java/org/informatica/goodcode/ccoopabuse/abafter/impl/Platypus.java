package org.informatica.goodcode.ccoopabuse.abafter.impl;

import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Beaked;
import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Living;

public class Platypus implements Living, Beaked {

    @Override
    public void canBreath() {
        // breath stuffs
    }

    @Override
    public void hasBeak() {
        // beak stuffs
    }

}
