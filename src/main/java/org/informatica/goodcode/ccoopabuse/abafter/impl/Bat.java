package org.informatica.goodcode.ccoopabuse.abafter.impl;

import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Bipedal;
import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Flyable;
import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Living;

public class Bat implements Bipedal, Living, Flyable {

    @Override
    public void canFly() {
        //fly stuffs
    }

    @Override
    public void canBreath() {
        // breath stuffs
    }

    @Override
    public void hasBipeds() {
        // leg stuffs
    }
}
