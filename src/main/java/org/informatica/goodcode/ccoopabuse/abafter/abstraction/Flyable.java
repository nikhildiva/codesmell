package org.informatica.goodcode.ccoopabuse.abafter.abstraction;

public interface Flyable {
    public void canFly();
}
