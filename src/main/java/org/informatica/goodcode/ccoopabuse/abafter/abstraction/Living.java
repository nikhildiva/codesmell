package org.informatica.goodcode.ccoopabuse.abafter.abstraction;

public interface Living {
    public abstract void canBreath();
}
