package org.informatica.goodcode.ccoopabuse.abafter.impl;

import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Bipedal;
import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Flyable;

public class Plane implements Flyable, Bipedal {
    @Override
    public void canFly() {

    }

    @Override
    public void hasBipeds() {

    }
}
