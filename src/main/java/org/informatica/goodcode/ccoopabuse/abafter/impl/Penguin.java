package org.informatica.goodcode.ccoopabuse.abafter.impl;

import org.informatica.goodcode.ccoopabuse.abafter.abstraction.Bird;

public class Penguin extends Bird {

    @Override
    public void canBreath() {
        // breath stuffs
    }

    @Override
    public void hasBeak() {
        // beak stuffs
    }

    @Override
    public void hasBipeds() {
        // leg stuffs
    }
}
