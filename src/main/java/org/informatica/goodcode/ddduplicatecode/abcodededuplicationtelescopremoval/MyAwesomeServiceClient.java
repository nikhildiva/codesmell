package org.informatica.goodcode.ddduplicatecode.abcodededuplicationtelescopremoval;

public class MyAwesomeServiceClient {
    String url;

    MyAwesomeServiceClient(String method, String host, String port, String version, boolean secured) {
        StringBuilder urlBuilder = new StringBuilder();

        urlBuilder.append(method == null ? "GET" : method).append(" ");

        urlBuilder.append(secured ? "https" : "http").append("://");

        urlBuilder.append(host == null ? "localhost" : host);

        urlBuilder.append(port == null ? "" : ":" + port);

        urlBuilder.append(version == null ? "v1" : version);

        url = urlBuilder.toString();
    }


    String makeRestCall() {

        return "success";
    }

}
