package org.informatica.goodcode.ddduplicatecode.adafterlongparamlistremoval;

public class MyAwesomeServiceUrlBuilder {
    String method;
    String host;
    String port;
    String version;
    String resource;
    String protocol;

    public String buildUrl() {
        StringBuilder urlBuilder = new StringBuilder();

        urlBuilder.append(method == null ? "GET" : method).append(" ");

        urlBuilder.append(protocol).append("://");

        urlBuilder.append(host == null ? "localhost" : host);

        urlBuilder.append(port == null ? "" : ":" + port);

        urlBuilder.append(version == null ? "v1" : version);

        return urlBuilder.toString();
    }

    public MyAwesomeServiceUrlBuilder method(String method) {
        this.method = method;
        return this;
    }

    public MyAwesomeServiceUrlBuilder host(String host) {
        this.host = host;
        return this;
    }

    public MyAwesomeServiceUrlBuilder port(String port) {
        this.port = port;
        return this;
    }

    public MyAwesomeServiceUrlBuilder version(String version) {
        this.version = version;
        return this;
    }

    public MyAwesomeServiceUrlBuilder protocol(String protocol) {
        this.protocol = protocol;
        return this;
    }


    public static void main(String[] args) {
        MyAwesomeServiceUrlBuilder myAwesomeServiceUrlBuilder = new MyAwesomeServiceUrlBuilder();


        MyAwesomeServiceUrlBuilder urlBuilder = myAwesomeServiceUrlBuilder.host("")
                .method("")
                .port("")
                .host("");

        new MyAwesomeServiceClient(urlBuilder).makeRestCall();

    }
}
