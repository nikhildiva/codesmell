package org.informatica.goodcode.ddduplicatecode.aabefore;

public class MyAwesomeServiceClient {
    String url;


    MyAwesomeServiceClient(String method) {
        url = method + " http://localhost/welcomePage";
    }

    MyAwesomeServiceClient(boolean secured, String method, String host) {
        String protocol = secured ? " https://" : " http://";
        url = method + protocol + host + "/v1" + "/welcomePage";
    }

    MyAwesomeServiceClient(boolean secured, String method, String host, String port) {
        String protocol = secured ? " https://" : " http://";
        url = method + protocol + host + ":" + port + "/v1" + "/welcomePage";
    }

    MyAwesomeServiceClient(boolean secured, String method, String host, String port, String version) {
        String protocol = secured ? " https://" : " http://";
        url = method + protocol + host + ":" + port + "/" + version + "/welcomePage";
    }

    MyAwesomeServiceClient(boolean secured, String method, String host, String port, String version, String uri) {
        String protocol = secured ? " https://" : " http://";
        url = method + protocol + host + ":" + port + "/" + version + "/" + uri;
    }

    String makeRestCall() {
        return url + " success";
    }
}
